#!/bin/bash

#############################################################################################
# 用途: Hexo-Async-Deployer 项目初始化脚本
# 作者: 丁辉
# 编写日期: 2021-10-1
# 更新日期: 2024-07-09
#############################################################################################

# 加载检测脚本
source <(curl -sS https://gitee.com/offends/Linux/raw/main/File/Shell/Check_command.sh)

# 初始化基础信息
function INIT_BASE_INFO() {
    # 修改基础模版
    sed -i "s#language: en#language: zh-Hans#g" /hexo/_config.yml
    sed -i "s#author: John Doe#author: Offends#g" /hexo/_config.yml

    SEND_INFO "初始化基础信息"
    # 本地搜索
    CHECK_COMMAND_NULL npm install hexo-generator-searchdb
    # 数字统计
    CHECK_COMMAND_NULL npm install hexo-wordcount
    # 初始化基础信息
    CHECK_COMMAND_NULL mv /app/_config.async.yml /hexo/_config.async.yml
    CHECK_DIR /hexo/source/_data/
    CHECK_COMMAND_NULL mv /app/links.yml /hexo/source/_data/links.yml
    CHECK_COMMAND_NULL mv /app/404.md /hexo/source/404.md
    CHECK_COMMAND_NULL mv /app/site.webmanifest /hexo/source/site.webmanifest
    CHECK_DIR /hexo/source/_data/style/
    CHECK_COMMAND_NULL mv /app/index.less /hexo/source/_data/style/index.less
}

# 初始化页面
function INIT_PAGE() {
    # 分类
    CHECK_COMMAND_NULL npm install hexo-generator-category
    # 页面名称
    PAGE_NAME=(
        "categories"
        "links"
        "about"
        "comment"
    )
    # 循环创建页面
    for name in ${PAGE_NAME[@]}; do
        # 创建页面
        CHECK_COMMAND_NULL hexo new page $name
        if [ $? -eq 0 ]; then
            # 创建页面成功
            SEND_INFO "创建页面 $name 成功"
        else
            # 创建页面失败
            SEND_WARN "创建页面 $name 失败"
        fi
    done

    # 修改页面内容
    # 友情链接
    sed -i 's#title: links#title: 友情链接#g' /hexo/source/links/index.md
    sed -i '3a\layout: links' /hexo/source/links/index.md
    # 关于
    sed -i 's#title: about#title: 关于#g' /hexo/source/about/index.md
    sed -i '3a\layout: about' /hexo/source/about/index.md
    sed -i '4a\single_column: false' /hexo/source/about/index.md
    sed -i '5a\comments: false' /hexo/source/about/index.md
    # 留言
    sed -i 's#title: comment#title: 留言#g' /hexo/source/comment/index.md
    sed -i '3a\layout: comment' /hexo/source/comment/index.md
    # 分类
    sed -i 's#title: categories#title: 分类#g' /hexo/source/categories/index.md
    sed -i '3a\layout: category' /hexo/source/categories/index.md
    sed -i '4a\single_column: true' /hexo/source/categories/index.md
    sed -i '5a\comments: false' /hexo/source/categories/index.md
}

# 初始化图片
function INIT_IMAGE() {
    # 存储地址
    MINIO_URL="https://minio.offends.cn:9000/offends/hexo-async"
    IMAGE_NAME=(
        "avatar.png"
        "favicon-16x16.png"
        "favicon-32x32.png"
        "block.png"
        "desktop.png"
        "wx.png"
        "zfb.png"
        "demo.png"
        "404.gif"
        "404.jpg"
    )
    CHECK_DIR /hexo/source/img
    # 循环下载页面
    for name in ${IMAGE_NAME[@]}; do
        # 下载图片
        CHECK_COMMAND_NULL curl -so /hexo/source/img/$name $MINIO_URL/$name
        
    done

    SEND_INFO "初始化图片成功"
}

# 收录配置
function INIT_BAIDU_SITEMAP() {
    SEND_INFO "初始化收录配置"
    ################################## 百度Sitemap 和 Google Sitemap ##################################
    CHECK_COMMAND_NULL npm install hexo-generator-sitemap
    CHECK_COMMAND_NULL npm install hexo-generator-baidu-sitemap
    sed -i "s#url: http://example.com#url: https://blog.offends.cn#g" /hexo/_config.yml
    sed -i '/^url:/ {N; s/\n/\nroot: \/\n/;}' /hexo/_config.yml
    sed -i 's/permalink: :year\/:month\/:day\/:title\//permalink: :title.html/' /hexo/_config.yml
    cat << EOF >> /hexo/_config.yml
sitemap:
  path: sitemap.xml
baidusitemap:
  path: baidusitemap.xml
EOF
    sed -i '1i<meta name="google-site-verification" content="V1iIzgHls93z_BAPofTvRD8jwh01u4Ls0fqXgGo86Ro" />' /hexo/node_modules/hexo-theme-async/layout/_partial/head.ejs
    sed -i '1i<meta name="baidu-site-verification" content="codeva-MZXYU3JS1x" />' /hexo/node_modules/hexo-theme-async/layout/_partial/head.ejs
    ################################## 百度主动推送 ##################################
    CHECK_COMMAND_NULL npm install hexo-baidu-url-submit
    sed -i '/deploy:/!b;n;s/  type: \x27\x27/- type: baidu_url_submitter/' /hexo/_config.yml
cat << EOF >> /hexo/_config.yml
baidu_url_submit:
  count: 200
  host: blog.offends.cn
  token: CRBfMzmp8EjAnQCl
  path: baidu_urls.txt
EOF
}

# 添加看板娘
function ADD_TOUCH_ICON() {
    SEND_INFO "添加看板娘"
    git clone https://githubfast.com/stevenjoezhang/live2d-widget.git /hexo/node_modules/hexo-theme-async/source/live2d-widget
    sed -i '1i<script src="/live2d-widget/autoload.js"></script>' /hexo/node_modules/hexo-theme-async/layout/_partial/head.ejs
    sed -i '1i<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome/css/font-awesome.min.css">' /hexo/node_modules/hexo-theme-async/layout/_partial/head.ejs
    sed -i '2s/^/\/\/ /; 3s/^\/\///' /hexo/node_modules/hexo-theme-async/source/live2d-widget/autoload.js
    sed -i 's/z-index: 1;/z-index: 9999;/g' /hexo/node_modules/hexo-theme-async/source/live2d-widget/waifu.css
    sed -i 's/e=1/e=5/g' /hexo/node_modules/hexo-theme-async/source/live2d-widget/waifu-tips.js
}

# 音乐位置 : 符号去除
# function INIT_MUSIC() {
#     sed -i 's/<%- __(item.key) %>:/<%- __(item.key) %>/g' /hexo/node_modules/hexo-theme-async/layout/_partial/sidebar/card/info.ejs
# }

# 执行全部初始化
function INIT_ALL() {
    # 初始化基础信息
    INIT_BASE_INFO
    # 初始化页面
    INIT_PAGE
    # 初始化图片
    INIT_IMAGE
    # 收录配置
    INIT_BAIDU_SITEMAP
    # 添加看板娘
    ADD_TOUCH_ICON
}

INIT_ALL