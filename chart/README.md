> 本文作者：丁辉

# Helm部署

开始部署

```bash
helm install blog ./chart \
  --namespace blog --create-namespace
```

卸载

```bash
helm uninstall blog -n blog
```

