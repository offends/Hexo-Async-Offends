{{/*
Startup parameter
*/}}
{{- define "argument" -}}
progressDeadlineSeconds: 200
replicas: 1
revisionHistoryLimit: 1
strategy:
  rollingUpdate:
    maxSurge: 25%
    maxUnavailable: 25%
  type: RollingUpdate
{{- end }}

{{- define "imagePullPolicy" -}}
imagePullPolicy: IfNotPresent
{{- end }}