# Hexo-Async-Offends

## 基础变量配置

|     变量名      |                        变量值                         |      备注      |
| :-------------: | :---------------------------------------------------: | :------------: |
| DOCKER_USERNAME |                                                       |  镜像仓库账号  |
| DOCKER_PASSWORD |                                                       |  镜像仓库密码  |
|    REGISTRY     |           registry.cn-hangzhou.aliyuncs.com           |  镜像仓库地址  |
|      REPO       | registry.cn-hangzhou.aliyuncs.com/<命名空间>/<镜像名> | 镜像的仓库名称 |

## 管理构建变量配置

| 变量名 |       变量值       |               备注                |
| :----: | :----------------: | :-------------------------------: |
| BUILD  | Hexo-async-offends | 构建 Async-Offends 初始化环境镜像 |
| BUILD  | Gateway-kubernetes |     构建 kubernetes 网关镜像      |
| BUILD  |   Gateway-docker   |       构建 docker 网关镜像        |
| BUILD  |        All         |      构建跟博客有关所有构建       |

